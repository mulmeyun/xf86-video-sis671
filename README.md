Doesn't work with latest Xorg 1.20, at least not in Archlinux.

For installing in Arch you need:
- xorg-server1.19-git
- xorg-server1.19-devel-git
- xf86-input-libinput <= 1.1.0-1

This version based on Archers yaourt sisimedia-0.10.3 pathed for use with xorg-server version > 1.12
adapted by rasdark (andrew.senik@gmail.com).

For xorg-1.12 just clone and:
```
git checkout for-xorg-1.12-1
```

For xorg-1.18 just clone and:
```
git checkout for-xorg-1.18
```

For xorg-1.19 just clone and:
```
git checkout for-xorg-1.19
```

In my arch box it works with:
```
autoreconf -vi
./configure --prefix=/usr --disable-static
make
sudo make install
```

Check if install is ok:
```
ls -la /usr/lib/xorg/modules/drivers/
```

You can use inxi too.
```
$ inxi -G
Graphics:  Card: Silicon Integrated Systems [SiS] 771/671 PCIE VGA Display Adapter
           Display Server: x11 (X.Org 1.19.3 ) driver: N/A Resolution: 1368x768@60.00hz
           OpenGL: renderer: Gallium 0.4 on softpipe version: 3.3 Mesa 17.0.6
```

For using the driver add in /etc/X11/xorg.conf:
```
Section "Device"
...
   Driver "sis671"
   Option "UseTiming1366" "yes"
...
EndSection
```
For using resolution 1366x768 on some notebooks (eg. ASUS K50C).

My personal config looks like:
```
Section "Device"
    Identifier "Configured Video Device"
    Driver "sis671"
EndSection

Section "Monitor"
    Identifier "Configured Monitor"
EndSection

Section "Screen"
    Identifier "Default Screen"
    Monitor "Configured Monitor"
    Device "Configured Video Device"
EndSection
```

It may also help to set the kernel parameter iomem=relaxed
```
linux   /boot/vmlinuz-linux root=UUID=5db4809 rw  quiet iomem=relaxed
```

Best regards.
